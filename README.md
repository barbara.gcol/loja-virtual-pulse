# Loja Virtual - Pulse

Projeto de WebService para Loja virtual com checkout transparente.

## Features
- REST API
- JWT authentication
- Pagination

## Tecnologias
**Backend**
  - Java
  - Spring Boot
  - Spring Security
  - JWT Authentication
  - Spring Data JPA
  - Hibernate
  - MySQL
  - Gradle

  ## Como Executar

Start the backend server before the frontend client.  

**Backend**

  1. Instalar [MySQL]
  2. Configure o arquivo `application.properties`.
  3. Acesse a pasta do projeto via terminal
  4. Execute o comando abaixo para fazer o build da app:
 
    # Linux
    ./gradlew clean build
      
    # Windows
    gradlew.bat clean build
  
  5. Run `gradle bootRun`
  6. O servidor backend rodará em [localhost:8080] (Os serviços se encontram no contexto /api). 

  # Serviços Disponíveis

  ## GET
  ### Recuperação da Listagem de Produtos

    # GET {{host-url}}/api/produtos
  
  ### Recuperar Informações de Produto

    #GET {{host-url}}/api/produto/{{idProduto}}

  ### Recuperação do Carrinho de Compras

    # GET {{host-url}}/api/carrinho

  ### Listar Pedidos do Usuário (Logado)

    # GET {{host-url}}/api/pedido/
    Authorization: barbara {{token}}

  ### Visualizar Pedido

    # GET {{host-url}}/api/pedido/{{idPedido}}
    Authorization: barbara {{token}}

  ### Finalizar Pedido

    # GET {{host-url}}/api/pedido/finish/{{idPedido}}
    Authorization: barbara {{token}}

  ### Cancelar Pedido

    # GET {{host-url}}/api/pedido/cancelar/{{idPedido}}
    Authorization: barbara {{token}}

  ### Recuperar Perfil de acesso do Usuário, através de seu email

    #GET {{host-url}}/api/profile/{{emailUsuario}}
    Authorization: barbara {{token}}

  ### Buscar Categoria por categoriaTipo

    #GET {{host-url}}/api/categoria/{{categoriaTipo}}

  ## POST

  ### Login 

    # POST {{host-url}}/login
    Content-Type: application/json

      {
        "login": {{email}},
        "senha": {{senha}},
        "rem": true
      }


  ### Adicionar Produto ao Carrinho de Compras

    # POST {{host-url}}/api/carrinho
    Content-Type: application/json

      {
        "idProduto": {{idProduto}},
        "quantidade": {{quantidade}}
      }
  
    ou 

    # POST {{host-url}}/api/carrinho/add
    Content-Type: application/json
    Authorization: barbara {{token}}

      {
        "idProduto": {{idProduto}},
        "quantidade": {{quantidade}}
      }

  ### Checkout do Carrinho de Compras
  
    # POST {{host-url}}/api/carrinho/checkout
    Content-Type: application/json
    Authorization: barbara {{token}}
  
  ### Cadastrar Novo Produto (Apenas com permissão de Admin)

    # POST {{host-url}}/api/vendedor/produto/novo
    Content-Type: application/json
    Authorization: barbara {{token}}
      {
        "idProduto": 1,
        "nome": "Livro X",
        "preco": 29.00,
        "estoque": 19,
        "descricao": "Livro de Estudo",
        "imagem": "https://images.jpg",
        "statusProduto": 0,
        "categoriaTipo": 1
      }

  ### Cadastro de Usuário

    # POST {{host-url}}/api/cadastrar
    Content-Type: application/json
    Authorization: barbara {{token}}

    {
      "id": 2147483641,
      "email": "customer1@email.com",
      "senha": "1234",
      "nome": "customer1",
      "tel": "6789",
      "endereco": "3200 West Road",
      "ativo": false,
      "perfil": "ROLE_CLIENT"
    }

  ## PUT
  
  ### Editar Produto (Apenas com permissão de Admin)

    # PUT {{host-url}}/api/vendedor/produto/{{idProduto}}/edit
    Content-Type: application/json
    Authorization: barbara {{token}}

      {
        "idProduto": 1,
        "nome": "Livro X",
        "preco": 29.00,
        "estoque": 19,
        "descricao": "Livro de Estudo",
        "imagem": "https://images.jpg",
        "statusProduto": 0,
        "categoriaTipo": 1
      }

  ### Editar Usuário

    # POST {{host-url}}/api/atualizar
    Content-Type: application/json
    Authorization: barbara {{token}}

    {
      "id": 2147483641,
      "email": "customer1@email.com",
      "senha": "1234",
      "nome": "customer1",
      "tel": "6789",
      "endereco": "3200 West Road",
      "ativo": false,
      "perfil": "ROLE_CLIENT"
    }

  ## DELETE 

  ### Excluir item do Carrinho de Compras
    
    # DELETE {{host-url}}/api/carrinho/{{idProduto}}
    Content-Type: application/json
    Authorization: barbara {{token}}

  ### Excluir Produto (Somente com permissão de Admin)

    # DELETE {{host-url}}/api/vendedor/produto/{{idProduto}}/dedlete
    Content-Type: application/json
    Authorization: barbara {{token}}

  

  

    

  


  

  






  