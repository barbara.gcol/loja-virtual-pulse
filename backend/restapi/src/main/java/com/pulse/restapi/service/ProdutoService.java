package com.pulse.restapi.service;

import com.pulse.restapi.model.Produto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author Bárbara Lopes
 */
public interface ProdutoService {

    Produto findOne(Integer idProduto);

    Page<Produto> findUpAll(Pageable pageable);
    Page<Produto> findAll(Pageable pageable);
    Page<Produto> findAllInCategoria(Integer categoriaProduto, Pageable pageable);

    void aumentarEstoque(Integer idProduto, int quantidade);
    void diminuirEstoque(Integer idProduto, int quantidade);

    Produto esgotarProduto(Integer idProduto);
    Produto disponibilizarProduto(Integer idProduto);
    
    Produto update(Produto produto);
    Produto save(Produto produto);

    void delete(Integer idProduto);


}
