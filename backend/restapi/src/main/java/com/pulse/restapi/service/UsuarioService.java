package com.pulse.restapi.service;

import com.pulse.restapi.model.Usuario;
import java.util.Collection;

/**
 * @author Bárbara Lopes
 */
public interface UsuarioService {
    Usuario findOne(String email);

    Collection<Usuario> findByPerfil(String perfil);

    Usuario save(Usuario usuario);

    Usuario update(Usuario usuario);
}
