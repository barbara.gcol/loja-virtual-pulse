package com.pulse.restapi.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Bárbara Lopes
 * Entidade Pedido, que armazena informações do usuário e produtos do pedido
 */
@Entity
@Data
@NoArgsConstructor
@DynamicUpdate
public class Pedido implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idPedido;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "pedido")
    private Set<ItemPedido> itensPedido = new HashSet<>();

    @NotEmpty
    private String emailComprador;

    @NotEmpty
    private String nomeComprador;

    @NotEmpty
    private String telComprador;

    @Basic(optional = false)
	private Endereco enderecoComprador;

    @NotNull
    private BigDecimal valor;

    @NotNull
    @ColumnDefault("0") // Novo
    private Integer statusPedido;

    @NotNull
    @ColumnDefault("0") // Boleto
    private Integer tipoPagamento;

    @CreationTimestamp
    private LocalDateTime dataCriacao;

    @UpdateTimestamp
    private LocalDateTime dataUpdate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transportadora_id")
    @JsonIgnore
    private Pedido pedido;

    public Pedido(Usuario comprador) {
        this.emailComprador = comprador.getEmail();
        this.nomeComprador = comprador.getNome();
        this.telComprador = comprador.getTel();
        this.enderecoComprador = comprador.getEndereco();
        this.valor = comprador.getCarrinho().getItemsPedido().stream().map(item -> item.getPrecoProduto().multiply(new BigDecimal(item.getQuantidade())))
                .reduce(BigDecimal::add)
                .orElse(new BigDecimal(0));
        this.statusPedido = 0;

    }
}
