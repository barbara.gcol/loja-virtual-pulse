package com.pulse.restapi.controller;

import com.pulse.restapi.model.Produto;
import com.pulse.restapi.service.CategoriaService;
import com.pulse.restapi.service.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author Bárbara Lopes
 */
@CrossOrigin
@RestController
public class ProdutoController {
    @Autowired
    CategoriaService categoriaProdutoService;
    @Autowired
    ProdutoService produtoService;

    /**
     * Exibe Lista de produtos cadastrados
     * @param page
     * @param size
     * @return
     */
    @GetMapping("/produtos")
    public Page<Produto> findAll(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                     @RequestParam(value = "size", defaultValue = "3") Integer size) {
        PageRequest request = PageRequest.of(page - 1, size);
        return produtoService.findAll(request);
    }

    /**
     * Exibe detalhes do produto pelo id
     * @param idProduto
     * @return
     */
    @GetMapping("/produto/{idProduto}")
    public Produto showOne(@PathVariable("idProduto") Integer idProduto) {

        Produto produto = produtoService.findOne(idProduto);
        return produto;
    }

    /**
     * Cadastra novo produto
     * @param produto
     * @param bindingResult
     * @return
     */
    @PostMapping("/vendedor/produto/novo")
    public ResponseEntity create(@Valid @RequestBody Produto produto,
                                 BindingResult bindingResult) {
        Produto produtoIdExiste = produtoService.findOne(produto.getIdProduto());
        if (produtoIdExiste != null) {
            bindingResult
                    .rejectValue("idProduto", "error.product",
                            "There is already a product with the code provided");
        }
        if (bindingResult.hasErrors()) {
            return ResponseEntity.badRequest().body(bindingResult);
        }
        return ResponseEntity.ok(produtoService.save(produto));
    }

    /**
     * Edita produto
     * @param idProduto
     * @param produto
     * @param bindingResult
     * @return
     */
    @PutMapping("/vendedor/produto/{id}/edit")
    public ResponseEntity edit(@PathVariable("id") Integer idProduto,
                               @Valid @RequestBody Produto produto,
                               BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.badRequest().body(bindingResult);
        }
        if (!idProduto.equals(produto.getIdProduto())) {
            return ResponseEntity.badRequest().body("Id Not Matched");
        }

        return ResponseEntity.ok(produtoService.update(produto));
    }

    /**
     * Deleta Produto
     * @param idProduto
     * @return
     */
    @DeleteMapping("/vendedor/produto/{id}/delete")
    public ResponseEntity delete(@PathVariable("id") Integer idProduto) {
        produtoService.delete(idProduto);
        return ResponseEntity.ok().build();
    }

}
