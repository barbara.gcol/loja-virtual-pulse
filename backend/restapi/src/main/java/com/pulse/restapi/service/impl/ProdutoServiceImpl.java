package com.pulse.restapi.service.impl;

import com.pulse.restapi.model.Produto;

import com.pulse.restapi.model.enums.StatusProdutoEnum;
import com.pulse.restapi.model.enums.ResultEnum;
import com.pulse.restapi.exception.ApiException;
import com.pulse.restapi.repository.ProdutoRepository;
import com.pulse.restapi.service.CategoriaService;
import com.pulse.restapi.service.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Bárbara Lopes
 */
@Service
public class ProdutoServiceImpl implements ProdutoService {

    @Autowired
    ProdutoRepository produtoRepository;

    @Autowired
    CategoriaService categoriaService;

    // Recupera produto por id
    @Override
    public Produto findOne(Integer idProduto) {
        Produto produto = produtoRepository.findByIdProduto(idProduto);
        return produto;
    }

    // Recupera todos os produtos disponíveis para venda
    @Override
    public Page<Produto> findUpAll(Pageable pageable) {
        return produtoRepository.findAllByStatusProdutoOrderByIdProdutoAsc(StatusProdutoEnum.DISPONIVEL.getCodigo(), pageable);
    }

    // Recupera todos os produtos
    @Override
    public Page<Produto> findAll(Pageable pageable) {
        return produtoRepository.findAllByOrderByIdProduto(pageable);
    }

    // Recupera produtos da categoria
    @Override
    public Page<Produto> findAllInCategoria(Integer categoriaProduto, Pageable pageable) {
        return produtoRepository.findAllByCategoriaTipoOrderByIdProdutoAsc(categoriaProduto, pageable);
    }

    // Aumenta Estoque do produto
    @Override
    @Transactional
    public void aumentarEstoque(Integer idProduto, int quantidade) {
        Produto produto = findOne(idProduto);
        if (produto == null) throw new ApiException(ResultEnum.PRODUCT_NOT_EXIST);

        int novoEstoque = produto.getEstoque() + quantidade;
        produto.setEstoque(novoEstoque);
        produtoRepository.save(produto);
    }

    // Diminui Estoque do Produto
    @Override
    @Transactional
    public void diminuirEstoque(Integer idProduto, int quantidade) {
        Produto produto = findOne(idProduto);
        if (produto == null) throw new ApiException(ResultEnum.PRODUCT_NOT_EXIST);

        int novoEstoque = produto.getEstoque() - quantidade;
        if(novoEstoque <= 0) throw new ApiException(ResultEnum.PRODUCT_NOT_ENOUGH );

        produto.setEstoque(novoEstoque);
        produtoRepository.save(produto);
    }

    // Coloca o produto como indisponível para venda
    @Override
    @Transactional
    public Produto esgotarProduto(Integer idProduto) {
        Produto productInfo = findOne(idProduto);
        if (productInfo == null) throw new ApiException(ResultEnum.PRODUCT_NOT_EXIST);

        if (productInfo.getStatusProduto() == StatusProdutoEnum.INDISPONIVEL.getCodigo()) {
            throw new ApiException(ResultEnum.PRODUCT_STATUS_ERROR);
        }

        productInfo.setStatusProduto(StatusProdutoEnum.INDISPONIVEL.getCodigo());
        return produtoRepository.save(productInfo);
    }

    // Disponibiliza produto para venda
    @Override
    @Transactional
    public Produto disponibilizarProduto(Integer idProduto) {
        Produto produto = produtoRepository.findByIdProduto(idProduto);
        if (produto == null) throw new ApiException(ResultEnum.PRODUCT_NOT_EXIST);

        if (produto.getStatusProduto() == StatusProdutoEnum.DISPONIVEL.getCodigo()) {
            throw new ApiException(ResultEnum.PRODUCT_STATUS_ERROR);
        }
        produto.setStatusProduto(StatusProdutoEnum.DISPONIVEL.getCodigo());
        return produtoRepository.save(produto);
    }

    // Atualiza informações do produto
    @Override
    public Produto update(Produto produto) {
        categoriaService.findByCategoria(produto.getCategoriaTipo());
        if(produto.getStatusProduto() > 1) {
            throw new ApiException(ResultEnum.PRODUCT_STATUS_ERROR);
        }
        return produtoRepository.save(produto);
    }

    // Salva produto
    @Override
    public Produto save(Produto produto) {
        return update(produto);
    }

    // Deleta produto
    @Override
    public void delete(Integer idProduto) {
        Produto produto = findOne(idProduto);
        if (produto == null) throw new ApiException(ResultEnum.PRODUCT_NOT_EXIST);
        produtoRepository.delete(produto);

    }


}
