package com.pulse.restapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * @author Bárbara Lopes
 * Armazena informações do Item (Produto) do Pedido
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ItemPedido implements Serializable {
    private static final long serialVersionUID = 1L;

    public ItemPedido(Produto produto, Integer quantidade) {
        this.id = produto.getIdProduto();
        this.nomeProduto = produto.getNome();
        this.descricaoProduto = produto.getDescricao();
        this.imagemProduto = produto.getFoto();
        this.categoriaTipo = produto.getCategoriaTipo();
        this.precoProduto = produto.getPreco();
        this.estoqueProduto = produto.getEstoque();
        this.quantidade = quantidade;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JsonIgnore
    private Carrinho carrinho;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pedido_id")
    @JsonIgnore
    private Pedido pedido;

    @NotEmpty
    private Integer idProduto;

    @NotEmpty
    private String nomeProduto;

    @NotNull
    private String descricaoProduto;

    private String imagemProduto;

    @NotNull
    private Integer categoriaTipo;

    @NotNull
    private BigDecimal precoProduto;

    @Min(0)
    private Integer estoqueProduto;

    @Min(1)
    private Integer quantidade;


    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof ItemPedido)) {
            return false;
        }
        ItemPedido itemPedido = (ItemPedido) o;
        return Objects.equals(id, itemPedido.id) && Objects.equals(carrinho, itemPedido.carrinho) && Objects.equals(pedido, itemPedido.pedido) && Objects.equals(idProduto, itemPedido.idProduto) && Objects.equals(nomeProduto, itemPedido.nomeProduto) && Objects.equals(descricaoProduto, itemPedido.descricaoProduto) && Objects.equals(imagemProduto, itemPedido.imagemProduto) && Objects.equals(categoriaTipo, itemPedido.categoriaTipo) && Objects.equals(precoProduto, itemPedido.precoProduto) && Objects.equals(estoqueProduto, itemPedido.estoqueProduto) && Objects.equals(quantidade, itemPedido.quantidade);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, carrinho, pedido, idProduto, nomeProduto, descricaoProduto, imagemProduto, categoriaTipo, precoProduto, estoqueProduto, quantidade);
    }
    

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", carrinho='" + getCarrinho() + "'" +
            ", pedido='" + getPedido() + "'" +
            ", idProduto='" + getIdProduto() + "'" +
            ", nomeProduto='" + getNomeProduto() + "'" +
            ", descricaoProduto='" + getDescricaoProduto() + "'" +
            ", imagemProduto='" + getImagemProduto() + "'" +
            ", categoriaTipo='" + getCategoriaTipo() + "'" +
            ", precoProduto='" + getPrecoProduto() + "'" +
            ", estoqueProduto='" + getEstoqueProduto() + "'" +
            ", quantidade='" + getQuantidade() + "'" +
            "}";
    }

}
