package com.pulse.restapi.controller;


import com.pulse.restapi.model.Produto;
import com.pulse.restapi.model.Carrinho;
import com.pulse.restapi.model.ItemPedido;
import com.pulse.restapi.model.Usuario;
import com.pulse.restapi.form.FormItem;
import com.pulse.restapi.repository.ItemPedidoRepository;
import com.pulse.restapi.service.CarrinhoService;
import com.pulse.restapi.service.ItemPedidoService;
import com.pulse.restapi.service.ProdutoService;
import com.pulse.restapi.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Collection;
import java.util.Collections;

/**
 * @author Bárbara Lopes
 */
@CrossOrigin
@RestController
@RequestMapping("/carrinho")
public class CarrinhoController {
    @Autowired
    CarrinhoService carrinhoService;
    @Autowired
    UsuarioService usuarioService;
    @Autowired
    ProdutoService produtoService;
    @Autowired
    ItemPedidoService itemPedidoService;
    @Autowired
    ItemPedidoRepository itemPedidoRepository;

    /**
     * Faz merge do carrinho com os novos itens
     * @param itensPedido
     * @param principal
     * @return
     */
    @PostMapping("")
    public ResponseEntity<Carrinho> mergeCarrinho(@RequestBody Collection<ItemPedido> itensPedido, Principal principal) {
        Usuario usuario = usuarioService.findOne(principal.getName());
        try {
            carrinhoService.mergeCarrinho(itensPedido, usuario);
        } catch (Exception e) {
            ResponseEntity.badRequest().body("Merge Carrinho Falhou");
        }
        return ResponseEntity.ok(carrinhoService.getCarrinho(usuario));
    }

    /**
     *Recupera Carrinho de compras
    */
    @GetMapping("")
    public Carrinho getCarrinho(Principal principal) {
        Usuario usuario = usuarioService.findOne(principal.getName());
        return carrinhoService.getCarrinho(usuario);
    }

    // Adiciona item ao carrinho, juntamente à sua quantidade
    @PostMapping("/add")
    public boolean addToCarrinho(@RequestBody FormItem form, Principal principal) {
        
        Produto produto = produtoService.findOne(form.getIdProduto());
        try {
            mergeCarrinho(Collections.singleton(new ItemPedido(produto, form.getQuantidade())), principal);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * Altera item do pedido
     * @param idItem
     * @param quantidade
     * @param principal
     * @return
     */
    @PutMapping("/{idItem}")
    public ItemPedido modifcarItem(@PathVariable("itemId") Integer idItem, @RequestBody Integer quantidade, Principal principal) {
        Usuario usuario = usuarioService.findOne(principal.getName());
        itemPedidoService.update(idItem, quantidade, usuario);
        return itemPedidoService.findOne(idItem, usuario);
    }

    /**
     * Deleta item do pedido
     * @param idItem
     * @param principal
     */
    @DeleteMapping("/{idItem}")
    public void deleteItem(@PathVariable("idItem") Integer idItem, Principal principal) {
        Usuario usuario = usuarioService.findOne(principal.getName());
        carrinhoService.delete(idItem, usuario);
    }

    /**
     * Faz o checkout do carrinho de compras
     * @param principal
     * @return
     */
    @PostMapping("/checkout")
    public ResponseEntity checkout(Principal principal) {
        Usuario usuario = usuarioService.findOne(principal.getName());
        carrinhoService.checkout(usuario);
        return ResponseEntity.ok(null);
    }
}
