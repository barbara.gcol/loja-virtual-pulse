package com.pulse.restapi.model.enums;

import lombok.Getter;


/**
 * @author Bárbara Lopes
 * Enumerator indicativo do status do produto
 */
@Getter
public enum StatusProdutoEnum implements CodigoEnum{
    DISPONIVEL(0, "Disponível"), INDISPONIVEL(1, "Indisponível");
    
    private Integer codigo;
    private String message;

    StatusProdutoEnum(Integer codigo, String message) {
        this.codigo = codigo;
        this.message = message;
    }

    public String getStatus(Integer codigo) {

        for(StatusProdutoEnum statusEnum : StatusProdutoEnum.values()) {
            if(statusEnum.getCodigo() == codigo) return statusEnum.getMessage();
        }
        return "";
    }

    public Integer getCodigo() {
        return codigo;
    }
}
