package com.pulse.restapi.exception;

import com.pulse.restapi.model.enums.ResultEnum;

public class ApiException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    
    private Integer codigo;

    public ApiException(ResultEnum resultEnum) {
        super(resultEnum.getDescricao());

        this.codigo = resultEnum.getCodigo();
    }

    public ApiException(Integer codigo, String descricao) {
        super(descricao);
        this.codigo = codigo;
    }
}
