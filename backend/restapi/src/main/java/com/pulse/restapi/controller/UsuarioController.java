package com.pulse.restapi.controller;

import com.pulse.restapi.model.Usuario;
import com.pulse.restapi.security.JWT.JwtProvider;
import com.pulse.restapi.service.UsuarioService;
import com.pulse.restapi.request.LoginForm;
import com.pulse.restapi.response.JwtResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

/**
 * @author Bárbara Lopes
 */
@CrossOrigin
@RestController
public class UsuarioController {

    @Autowired
    UsuarioService usuarioService;

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    AuthenticationManager authenticationManager;

    /**
     * Realiza login
     * @param loginForm
     * @return
     */
    @PostMapping("/login")
    public ResponseEntity<JwtResponse> login(@RequestBody LoginForm loginForm) {
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginForm.getLogin(), loginForm.getSenha()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtProvider.generate(authentication);
            UserDetails detalhesUsuario = (UserDetails) authentication.getPrincipal();
            Usuario usuario = usuarioService.findOne(detalhesUsuario.getUsername());
            return ResponseEntity.ok(new JwtResponse(jwt, usuario.getEmail(), usuario.getNome(), usuario.getPerfil()));
        } catch (AuthenticationException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    /**
     * Cadastra novo usuário
     * @param usuario
     * @return
     */
    @PostMapping("/cadastrar")
    public ResponseEntity<Usuario> save(@RequestBody Usuario usuario) {
        try {
            return ResponseEntity.ok(usuarioService.save(usuario));
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

    /**
     * Atualiza informações do usuário
     * @param usuario
     * @param principal
     * @return
     */
    @PutMapping("/atualizar")
    public ResponseEntity<Usuario> update(@RequestBody Usuario usuario, Principal principal) {

        try {
            if (!principal.getName().equals(usuario.getEmail())) throw new IllegalArgumentException();
            return ResponseEntity.ok(usuarioService.update(usuario));
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

    /**
     * Recupera perfil de acesso do usuário, via email do usuário
     * @param email
     * @param principal
     * @return
     */
    @GetMapping("/profile/{email}")
    public ResponseEntity<Usuario> getProfile(@PathVariable("email") String email, Principal principal) {
        if (principal.getName().equals(email)) {
            return ResponseEntity.ok(usuarioService.findOne(email));
        } else {
            return ResponseEntity.badRequest().build();
        }

    }
}
