package com.pulse.restapi.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

import javax.validation.constraints.NotNull;

/**
 * @author Bárbara Lopes
 * Entidade Endereço, que armazena as informações de endereço do comprador, para envio dos pedidos
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Endereco implements Serializable {
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private static final long serialVersionUID = 1L;

    private String rua;
    private String numero;
    private int cep;
    private String cidade;
    private String estado;
    private String pais;


    @Override
    public String toString() {
        return "{" +
            " rua='" + getRua() + "'" +
            ", numero='" + getNumero() + "'" +
            ", cep='" + getCep() + "'" +
            ", cidade='" + getCidade() + "'" +
            ", estado='" + getEstado() + "'" +
            ", pais='" + getPais() + "'" +
            "}";
    }

}
