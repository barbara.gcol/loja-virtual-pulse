package com.pulse.restapi.repository;

import com.pulse.restapi.model.ItemPedido;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Bárbara Lopes
 */
public interface ItemPedidoRepository extends JpaRepository<ItemPedido, Integer> {

}
