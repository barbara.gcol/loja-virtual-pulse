package com.pulse.restapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author barbaralopes
 * @project restapi
 * @date 09/19/2020
 **/
@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException
{

	private static final long serialVersionUID = 1L;
	
	public ResourceNotFoundException( String exception )
	{
		super( exception );
	}

	public ResourceNotFoundException()
	{
		super();
	}

	public ResourceNotFoundException( String message, Throwable cause )
	{
		super( message, cause );
	}

}
