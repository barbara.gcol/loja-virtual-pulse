package com.pulse.restapi.service.impl;

import com.pulse.restapi.model.Carrinho;
import com.pulse.restapi.model.Pedido;
import com.pulse.restapi.model.ItemPedido;
import com.pulse.restapi.model.Usuario;
import com.pulse.restapi.repository.CarrinhoRepository;
import com.pulse.restapi.repository.PedidoRepository;
import com.pulse.restapi.repository.ItemPedidoRepository;
import com.pulse.restapi.repository.UsuarioRepository;
import com.pulse.restapi.service.CarrinhoService;
import com.pulse.restapi.service.ProdutoService;
import com.pulse.restapi.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

@Service
public class CarrinhoServiceImpl implements CarrinhoService {
    @Autowired
    ProdutoService produtoService;
    @Autowired
    PedidoRepository pedidoRepository;
    @Autowired
    UsuarioRepository usurioRepository;

    @Autowired
    ItemPedidoRepository itemPedidoRepository;
    @Autowired
    CarrinhoRepository carrinhoRepository;
    @Autowired
    UsuarioService usuarioService;

    @Override
    public Carrinho getCarrinho(Usuario usuario) {
        return usuario.getCarrinho();
    }

    @Override
    @Transactional
    public void mergeCarrinho(Collection<ItemPedido> itemsPedido, Usuario usuario) {
        Carrinho carrinhoFinal = usuario.getCarrinho();
        itemsPedido.forEach(itemPedido -> {
            Set<ItemPedido> set = carrinhoFinal.getItemsPedido();
            Optional<ItemPedido> old = set.stream().filter(e -> e.getIdProduto().equals(itemPedido.getIdProduto())).findFirst();
            ItemPedido prod;
            if (old.isPresent()) {
                prod = old.get();
                prod.setQuantidade(itemPedido.getQuantidade() + prod.getQuantidade());
            } else {
                prod = itemPedido;
                prod.setCarrinho(carrinhoFinal);
                carrinhoFinal.getItemsPedido().add(prod);
            }
            itemPedidoRepository.save(prod);
        });
        carrinhoRepository.save(carrinhoFinal);

    }

    @Override
    @Transactional
    public void delete(Integer itemId, Usuario usuario) {
        Optional<ItemPedido> item = usuario.getCarrinho().getItemsPedido().stream().filter(e -> itemId.equals(e.getIdProduto())).findFirst();
        item.ifPresent(itemPedido -> {
            itemPedido.setCarrinho(null);
            itemPedidoRepository.deleteById(itemPedido.getId());
        });
    }

    @Override
    @Transactional
    public void checkout(Usuario usuario) {
        Pedido pedido = new Pedido(usuario);
        pedidoRepository.save(pedido);

        usuario.getCarrinho().getItemsPedido().forEach(itemPedido -> {
            itemPedido.setCarrinho(null);
            itemPedido.setPedido(pedido);
            produtoService.diminuirEstoque(itemPedido.getIdProduto(), itemPedido.getQuantidade());
            itemPedidoRepository.save(itemPedido);
        });

    }
}
