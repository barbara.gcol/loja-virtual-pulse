package com.pulse.restapi.model;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import javax.persistence.Id;

import java.io.Serializable;

import org.hibernate.annotations.NaturalId;
import java.util.Date;

/**
 * @author Bárbara Lopes
 * Categoria do Produto
 */
@Entity
@Data
@DynamicUpdate
public class Categoria implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Integer categoriaId;

    private String categoriaNome;

    @NaturalId
    private Integer categoriaTipo;

    private Date dataCriacao;

    private Date dataUpdate;


    public Categoria() {
    }

    public Categoria(String categoriaNome, Integer categoriaTipo) {
        this.categoriaNome = categoriaNome;
        this.categoriaTipo = categoriaTipo;
    }
}
