package com.pulse.restapi.repository;

import com.pulse.restapi.model.Produto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Bárbara Lopes
 */
public interface ProdutoRepository extends JpaRepository<Produto, Integer> {
    Produto findByIdProduto(Integer id);
    Page<Produto> findAllByStatusProdutoOrderByIdProdutoAsc(Integer statusProduto, Pageable pageable);
    Page<Produto> findAllByCategoriaTipoOrderByIdProdutoAsc(Integer categoriaTipo, Pageable pageable);
    Page<Produto> findAllByOrderByIdProduto(Pageable pageable);

}
