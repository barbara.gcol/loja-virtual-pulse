package com.pulse.restapi.repository;

import com.pulse.restapi.model.Categoria;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Bárbara Lopes
 */
public interface CategoriaRepository extends JpaRepository<Categoria, Integer> {    
    List<Categoria> findByCategoriaTipoInOrderByCategoriaTipoAsc(List<Integer> categoriaTipos);
    List<Categoria> findAllByOrderByCategoriaTipo();
    Categoria findByCategoriaTipo(Integer categoriaTipo);
}
