package com.pulse.restapi.controller;


import com.pulse.restapi.model.Pedido;
import com.pulse.restapi.model.ItemPedido;
import com.pulse.restapi.service.PedidoService;
import com.pulse.restapi.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 * @author Bárbara Lopes
 */
@RestController
@CrossOrigin
public class PedidoController {
    @Autowired
    PedidoService pedidoService;
    @Autowired
    UsuarioService usuarioService;

    /**
     * Lista pedidos do usuário logado
     * @param page
     * @param size
     * @param authentication
     * @return
     */
    @GetMapping("/pedido")
    public Page<Pedido> listaPedidos(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                     @RequestParam(value = "size", defaultValue = "10") Integer size,
                                     Authentication authentication) {
        PageRequest request = PageRequest.of(page - 1, size);
        Page<Pedido> paginaPedido;
        if (authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_CLIENT"))) {
            paginaPedido = pedidoService.findByEmailComprador(authentication.getName(), request);
        } else {
            paginaPedido = pedidoService.findAll(request);
        }
        return paginaPedido;
    }

    /**
     * Cancela Pedido
     * @param idPedido
     * @param authentication
     * @return
     */
    @PatchMapping("/pedido/cancelar/{id}")
    public ResponseEntity<Pedido> cancel(@PathVariable("id") Integer idPedido, Authentication authentication) {
        Pedido pedido = pedidoService.findOne(idPedido);
        if (!authentication.getName().equals(pedido.getEmailComprador()) && authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_CLIENT"))) {

            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        return ResponseEntity.ok(pedidoService.cancel(idPedido));
    }

    /**
     * Finaliza Pedido
     * @param idPedido
     * @param authentication
     * @return
     */
    @PatchMapping("/pedido/finish/{id}")
    public ResponseEntity<Pedido> finish(@PathVariable("id") Integer idPedido, Authentication authentication) {
        if (authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_CLIENT"))) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        return ResponseEntity.ok(pedidoService.finish(idPedido));
    }

    /**
     * Exibe informações do pedido
     * @param orderId
     * @param authentication
     * @return
     */
    @GetMapping("/pedido/{id}")
    public ResponseEntity show(@PathVariable("id") Integer orderId, Authentication authentication) {
        boolean isCustomer = authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_CLIENT"));
        Pedido pedido = pedidoService.findOne(orderId);
        if (isCustomer && !authentication.getName().equals(pedido.getEmailComprador())) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        Collection<ItemPedido> items = pedido.getItensPedido();
        return ResponseEntity.ok(pedido);
    }
}
