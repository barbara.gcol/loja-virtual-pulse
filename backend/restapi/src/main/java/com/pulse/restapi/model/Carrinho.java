package com.pulse.restapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Bárbara Lopes 
 * Carrinho de Compras do Usuário
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Carrinho implements Serializable {
    
    private static final long serialVersionUID = 1L;

    public Carrinho(Usuario usuario) {
        this.usuario  = usuario;
    }

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    @JsonIgnore
    private Usuario usuario;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "carrinho")
    private Set<ItemPedido> itemsPedido = new HashSet<>();

    @Override
    public String toString() {
        return "Carrinho{ id=" + id + ", produtos=" + itemsPedido + '}';
    }
}
