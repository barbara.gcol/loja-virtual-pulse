package com.pulse.restapi.model;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import javax.persistence.Id;

/**
 * @author Bárbara Lopes
 * Entidade Transportadora
 */
@Entity
@Data
@DynamicUpdate
public class Transportadora {

    @Id
    @GeneratedValue
    private Integer id;
    private String nome;
    private double frete;

}
