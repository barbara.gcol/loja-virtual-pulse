package com.pulse.restapi.service.impl;


import com.pulse.restapi.model.Pedido;
import com.pulse.restapi.model.ItemPedido;
import com.pulse.restapi.model.Produto;
import com.pulse.restapi.model.enums.StatusPedidoEnum;
import com.pulse.restapi.model.enums.ResultEnum;
import com.pulse.restapi.exception.ApiException;
import com.pulse.restapi.repository.PedidoRepository;
import com.pulse.restapi.repository.ItemPedidoRepository;
import com.pulse.restapi.repository.ProdutoRepository;
import com.pulse.restapi.repository.UsuarioRepository;
import com.pulse.restapi.service.PedidoService;
import com.pulse.restapi.service.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PedidoServiceImpl implements PedidoService {
    @Autowired
    PedidoRepository pedidoRepository;
    @Autowired
    UsuarioRepository usuarioRepository;
    @Autowired
    ProdutoRepository produtoRepository;
    @Autowired
    ProdutoService produtoService;
    @Autowired
    ItemPedidoRepository itemPedidoRepository;

    @Override
    public Page<Pedido> findAll(Pageable pageable) {
        return pedidoRepository.findAll(pageable);
    }

    @Override
    public Page<Pedido> findByStatus(Integer statusPedido, Pageable pageable) {
        return pedidoRepository.findAllByStatusPedidoOrderByDataCriacaoDesc(statusPedido, pageable);
    }

    @Override
    public Page<Pedido> findByEmailComprador(String email, Pageable pageable) {
        return pedidoRepository.findAllByEmailCompradorOrderByStatusPedidoAscDataCriacaoDesc(email, pageable);
    }

    @Override
    public Page<Pedido> findByTelComprador(String phone, Pageable pageable) {
        return pedidoRepository.findAllByTelCompradorOrderByStatusPedidoAscDataCriacaoDesc(phone, pageable);
    }

    @Override
    public Pedido findOne(Integer idPedido) {
        Pedido pedido = pedidoRepository.findByIdPedido(idPedido);
        if(pedido == null) {
            throw new ApiException(ResultEnum.ORDER_NOT_FOUND);
        }
        return pedido;
    }

    @Override
    @Transactional
    public Pedido finish(Integer idPedido) {
        Pedido pedido = findOne(idPedido);
        if(!pedido.getStatusPedido().equals(StatusPedidoEnum.NOVO.getCodigo())) {
            throw new ApiException(ResultEnum.ORDER_STATUS_ERROR);
        }

        pedido.setStatusPedido(StatusPedidoEnum.FINALIZADO.getCodigo());
        pedidoRepository.save(pedido);
        return pedidoRepository.findByIdPedido(idPedido);
    }

    @Override
    @Transactional
    public Pedido cancel(Integer idPedido) {
        Pedido pedido = findOne(idPedido);
        if(!pedido.getStatusPedido().equals(StatusPedidoEnum.NOVO.getCodigo())) {
            throw new ApiException(ResultEnum.ORDER_STATUS_ERROR);
        }

        pedido.setStatusPedido(StatusPedidoEnum.CANCELADO.getCodigo());
        pedidoRepository.save(pedido);

        // Restore Stock
        Iterable<ItemPedido> produtos = pedido.getItensPedido();
        for(ItemPedido itemPedido : produtos) {
            Produto produto = produtoRepository.findByIdProduto(itemPedido.getIdProduto());
            if(produto != null) {
                produtoService.aumentarEstoque(itemPedido.getIdProduto(), itemPedido.getQuantidade());
            }
        }
        return pedidoRepository.findByIdPedido(idPedido);

    }
}
