package com.pulse.restapi.model.enums;

/**
 * @author Bárbara Lopes
 * Enumerator Para seleção de tipo de pagamento
 */
public enum TipoPagamentoEnum implements CodigoEnum {
    BOLETO(0, "Boleto"),
    CARTAO(1, "Cartão");

    private  int codigo;
    private String descricao;

    TipoPagamentoEnum(Integer codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    @Override
    public Integer getCodigo() {
        return codigo;
    }
}
