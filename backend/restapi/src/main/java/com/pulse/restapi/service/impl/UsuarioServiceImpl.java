package com.pulse.restapi.service.impl;

import com.pulse.restapi.model.Carrinho;
import com.pulse.restapi.model.Usuario;
import com.pulse.restapi.model.enums.ResultEnum;

import com.pulse.restapi.exception.ApiException;
import com.pulse.restapi.repository.CarrinhoRepository;
import com.pulse.restapi.repository.UsuarioRepository;
import com.pulse.restapi.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

/**
 * @author Bárbara Lopes
 */
@Service
@DependsOn("passwordEncoder")
public class UsuarioServiceImpl implements UsuarioService {
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    UsuarioRepository usuarioRepository;
    @Autowired
    CarrinhoRepository carrinhoRepository;

    /**
     * Recupera Usuario pelo email
     */
    @Override
    public Usuario findOne(String email) {
        return usuarioRepository.findByEmail(email);
    }

    // Recupera usuários a partir do perfil
    @Override
    public Collection<Usuario> findByPerfil(String perfil) {
        return usuarioRepository.findAllByPerfil(perfil);
    }

    // Salva Usuário
    @Override
    @Transactional
    public Usuario save(Usuario usuario) {
        usuario.setSenha(passwordEncoder.encode(usuario.getSenha()));
        try {
            Usuario usuarioSalvo = usuarioRepository.save(usuario);

            // Salva carrinho do usuário
            Carrinho carrinhoSalvo = carrinhoRepository.save(new Carrinho(usuarioSalvo));
            usuarioSalvo.setCarrinho(carrinhoSalvo);
            return usuarioRepository.save(usuarioSalvo);

        } catch (Exception e) {
            throw new ApiException(ResultEnum.VALID_ERROR);
        }
    }

    // Atualiza usuário
    @Override
    @Transactional
    public Usuario update(Usuario usuario) {
        Usuario usuarioAntigo = usuarioRepository.findByEmail(usuario.getEmail());
        usuarioAntigo.setSenha(passwordEncoder.encode(usuario.getSenha()));
        usuarioAntigo.setNome(usuario.getNome());
        usuarioAntigo.setTel(usuario.getTel());
        usuarioAntigo.setEndereco(usuario.getEndereco());
        return usuarioRepository.save(usuarioAntigo);
    }
}
