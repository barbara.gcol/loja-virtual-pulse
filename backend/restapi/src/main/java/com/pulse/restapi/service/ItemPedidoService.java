package com.pulse.restapi.service;

import com.pulse.restapi.model.ItemPedido;
import com.pulse.restapi.model.Usuario;

/**
 * @author Bárbara Lopes
 */
public interface ItemPedidoService {
    void update(Integer idItem, Integer quantidade, Usuario usuario);
    ItemPedido findOne(Integer idItem, Usuario usuario);
}
