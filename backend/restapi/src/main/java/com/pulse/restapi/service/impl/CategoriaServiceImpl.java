package com.pulse.restapi.service.impl;

import com.pulse.restapi.model.Categoria;
import com.pulse.restapi.model.enums.ResultEnum;
import com.pulse.restapi.exception.ApiException;
import com.pulse.restapi.repository.CategoriaRepository;
import com.pulse.restapi.service.CategoriaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CategoriaServiceImpl implements CategoriaService {
    @Autowired
    CategoriaRepository categoriaRepository;

    @Override
    public List<Categoria> findAll() {
        List<Categoria> res = categoriaRepository.findAll();
        return res;
    }

    @Override
    public Categoria findByCategoria(Integer categoriaProduto) {
        Categoria res = categoriaRepository.findByCategoriaTipo(categoriaProduto);
        if(res == null) throw new ApiException(ResultEnum.CATEGORY_NOT_FOUND);
        return res;
    }

    
    @Override
    public List<Categoria> findByCategoriaIn(List<Integer> categoriaProdutos) {
        List<Categoria> res = categoriaRepository.findByCategoriaTipoInOrderByCategoriaTipoAsc(categoriaProdutos);
        return res;
    }

    @Override
    @Transactional
    public Categoria save(Categoria categoriaProduto) {
        return categoriaRepository.save(categoriaProduto);
    }
}
