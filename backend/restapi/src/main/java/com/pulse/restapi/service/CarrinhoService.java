package com.pulse.restapi.service;

import com.pulse.restapi.model.Carrinho;
import com.pulse.restapi.model.ItemPedido;
import com.pulse.restapi.model.Usuario;
import java.util.Collection;

/**
 * @author Bárbara Lopes
 */
public interface CarrinhoService {
    Carrinho getCarrinho(Usuario usuario);

    void mergeCarrinho(Collection<ItemPedido> itemsPedido, Usuario usuario);

    void delete(Integer itemId, Usuario usuario);

    void checkout(Usuario usuario);
}
