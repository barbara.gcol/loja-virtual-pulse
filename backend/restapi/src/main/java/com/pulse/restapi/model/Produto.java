package com.pulse.restapi.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Bárbara Lopes
 * Entidade Produto
 */
@Entity
@Data
@NoArgsConstructor
@DynamicUpdate
public class Produto implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idProduto;

    @NotNull
    private String nome;

    @NotNull
    private BigDecimal preco;

    @NotNull
    @Min(0)
    private Integer estoque;

    private String descricao;

    private String foto;

    @ColumnDefault("0")
    private Integer statusProduto;

    @ColumnDefault("0")
    private Integer categoriaTipo;

    @CreationTimestamp
    private Date dataCriacao;
    @UpdateTimestamp
    private Date dataUpdate;

}
