package com.pulse.restapi.response;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * @author barbaralopes
 * @project restapi
 * @date 09/19/2020
 **/
@SuppressWarnings("unchecked")
public class ResponseWrapper<T> extends ResponseEntity<T>
{
	public ResponseWrapper( T t, HttpStatus status )
	{
		super( ( T ) new Resultset<>( t, status ), status );
	}
}
