package com.pulse.restapi.repository;

import com.pulse.restapi.model.Carrinho;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Bárbara Lopes
 */
public interface CarrinhoRepository extends JpaRepository<Carrinho, Integer> {}
