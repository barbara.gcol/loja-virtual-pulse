package com.pulse.restapi.controller;


import com.pulse.restapi.model.Categoria;
import com.pulse.restapi.model.Produto;
import com.pulse.restapi.service.CategoriaService;
import com.pulse.restapi.service.ProdutoService;
import com.pulse.restapi.response.PageCategoria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

/**
 * @author Bárbara Lopes
 */
@RestController
@CrossOrigin
public class CategoriaController {
    @Autowired
    CategoriaService categoriaProdutoService;
    @Autowired
    ProdutoService produtoService;

    /**
     * Exibe nome da categoria pelo tipo
     *
     * @param categoriaProduto
     * @param page
     * @param size
     * @return
     */
    @GetMapping("/categoria/{tipo}")
    public PageCategoria showOne(@PathVariable("tipo") Integer categoriaProduto,
                                @RequestParam(value = "page", defaultValue = "1") Integer page,
                                @RequestParam(value = "size", defaultValue = "3") Integer size) {

        Categoria cat = categoriaProdutoService.findByCategoria(categoriaProduto);
        PageRequest request = PageRequest.of(page - 1, size);
        Page<Produto> productInCategory = produtoService.findAllInCategoria(categoriaProduto, request);
        PageCategoria tmp = new PageCategoria("", productInCategory);
        tmp.setCategory(cat.getCategoriaNome());
        return tmp;
    }
}
