package com.pulse.restapi.form;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Data
public class FormItem {
    @Min(value = 1)
    private Integer quantidade;
    @NotEmpty
    private Integer idProduto;
    
}
