package com.pulse.restapi.model.enums;

/**
 * @author Bárbara Lopes
 * Enumerator indicativo do status do pedido
 */
public enum StatusPedidoEnum implements CodigoEnum {
    NOVO(0, "Novo"),
    FINALIZADO(1, "Finalizado"),
    CANCELADO(2, "Cancelado")
    ;

    private  int codigo;
    private String descricao;

    StatusPedidoEnum(Integer codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    @Override
    public Integer getCodigo() {
        return codigo;
    }
}
