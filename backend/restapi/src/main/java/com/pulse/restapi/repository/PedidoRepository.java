package com.pulse.restapi.repository;

import com.pulse.restapi.model.Pedido;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Bárbara Lopes
 */
public interface PedidoRepository extends JpaRepository<Pedido, Integer> {
    Pedido findByIdPedido(Integer orderId);
    Page<Pedido> findAllByStatusPedidoOrderByDataCriacaoDesc(Integer statusPedido, Pageable pageable);
    Page<Pedido> findAllByEmailCompradorOrderByStatusPedidoAscDataCriacaoDesc(String emailComprador, Pageable pageable);
    Page<Pedido> findAllByOrderByStatusPedidoAscDataCriacaoDesc(Pageable pageable);
    Page<Pedido> findAllByTelCompradorOrderByStatusPedidoAscDataCriacaoDesc(String telComprador, Pageable pageable);
}
