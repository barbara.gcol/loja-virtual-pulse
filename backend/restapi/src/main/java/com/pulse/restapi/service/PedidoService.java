package com.pulse.restapi.service;

import com.pulse.restapi.model.Pedido;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author Bárbara Lopes
 */
public interface PedidoService {
    Page<Pedido> findAll(Pageable pageable);

    Page<Pedido> findByStatus(Integer statusPedido, Pageable pageable);

    Page<Pedido> findByEmailComprador(String email, Pageable pageable);

    Page<Pedido> findByTelComprador(String tel, Pageable pageable);

    Pedido findOne(Integer idPedido);

    Pedido finish(Integer idPedido);
    
    Pedido cancel(Integer idPedido);

}
