package com.pulse.restapi.service;

import com.pulse.restapi.model.Categoria;

import java.util.List;

/**
 * @author Bárbara Lopes
 */
public interface CategoriaService {

    List<Categoria> findAll();

    Categoria findByCategoria(Integer idCategoria);

    List<Categoria> findByCategoriaIn(List<Integer> listaCategorias);

    Categoria save(Categoria categoria);


}
