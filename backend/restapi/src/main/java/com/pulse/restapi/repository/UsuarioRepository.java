package com.pulse.restapi.repository;

import com.pulse.restapi.model.Usuario;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

/**
 * @author Bárbara Lopes
 */
public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {
    Usuario findByEmail(String email);
    Collection<Usuario> findAllByPerfil(String perfil);

}
