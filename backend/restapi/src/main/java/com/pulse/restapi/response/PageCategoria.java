package com.pulse.restapi.response;

import com.pulse.restapi.model.Produto;
import org.springframework.data.domain.Page;

public class PageCategoria {
    private String categoriaProduto;
    private Page<Produto> page;

    public PageCategoria(String categoriaProduto, Page<Produto> page) {
        this.categoriaProduto = categoriaProduto;
        this.page = page;
    }

    public String getCategory() {
        return categoriaProduto;
    }

    public void setCategory(String categoriaProduto) {
        this.categoriaProduto = categoriaProduto;
    }

    public Page<Produto> getPage() {
        return page;
    }

    public void setPage(Page<Produto> page) {
        this.page = page;
    }
}
