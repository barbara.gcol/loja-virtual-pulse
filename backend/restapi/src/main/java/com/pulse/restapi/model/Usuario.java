package com.pulse.restapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Bárbara Lopes
 * Entidade Usuário
 */
@Entity
@Data
@NoArgsConstructor
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NaturalId
    @NotEmpty
    private String email;

    @NotEmpty
    @Size(min = 3, message = "Length must be more than 3")
    private String senha;
    
    @NotEmpty
    private String nome;
    
    @NotEmpty
    private String tel;
    
    @Basic(optional = false)
	private Endereco endereco;
    
    @NotNull
    private boolean ativo;
    
    @NotEmpty
    private String perfil = "ROLE_CLIENT";

    @ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "perfis")
	private Set<Integer> perfis = new HashSet<>();

    @OneToOne(mappedBy = "usuario", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private Carrinho carrinho;

    @Override
    public String toString() {
        return "Cliente{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + senha + '\'' +
                ", name='" + nome + '\'' +
                ", phone='" + tel + '\'' +
                ", address='" + endereco + '\'' +
                ", active=" + ativo +
                ", role='" + perfil + '\'' +
                '}';
    }

}

