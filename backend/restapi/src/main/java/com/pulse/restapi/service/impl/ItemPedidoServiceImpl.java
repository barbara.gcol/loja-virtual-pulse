package com.pulse.restapi.service.impl;

import com.pulse.restapi.model.ItemPedido;
import com.pulse.restapi.model.Usuario;
import com.pulse.restapi.repository.ItemPedidoRepository;
import com.pulse.restapi.service.ItemPedidoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.atomic.AtomicReference;

import java.util.Optional;

@Service
public class ItemPedidoServiceImpl implements ItemPedidoService {

    @Autowired
    ItemPedidoRepository itemPedidoRepository;

    @Override
    @Transactional
    public void update(Integer idItem, Integer quantidade, Usuario usuario) {
        Optional<ItemPedido> op = usuario.getCarrinho().getItemsPedido().stream().filter(e -> idItem.equals(e.getIdProduto())).findFirst();
        op.ifPresent(itemPedido -> {
            itemPedido.setQuantidade(quantidade);
            itemPedidoRepository.save(itemPedido);
        });

    }

    @Override
    public ItemPedido findOne(Integer itemId, Usuario usuario) {
        Optional<ItemPedido> op = usuario.getCarrinho().getItemsPedido().stream().filter(e -> itemId.equals(e.getIdProduto())).findFirst();
        AtomicReference<ItemPedido> res = new AtomicReference<>();
        op.ifPresent(res::set);
        return res.get();
    }
}
